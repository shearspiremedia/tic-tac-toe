import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-gameinfo',
  templateUrl: './gameinfo.component.html',
  styleUrls: ['./gameinfo.component.css']
})
export class GameinfoComponent {
  @Input() infoprops: {};
  @Output() stepclicked = new EventEmitter<number>();

  stepClick(i: number): void {
    this.stepclicked.emit(i);
  }
}
