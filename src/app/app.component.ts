import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Tic-Tac-Toe';
  status = 'Next player: X';
  state;
  currentsquares;
  moves;
  boardprops;
  infoprops;

  handleClick(i): void {
    const history: any[] = this.state.history.slice(0, this.state.stepNumber + 1);
    const current = history[history.length - 1];
    const squares: any[] = current.squares.slice();
    if (this.calculateWinner(squares) || squares[i]){
        return;
    }
    const colrows = this.state.colrows.slice(0, this.state.stepNumber + 1);
    const colrow = [i % 3, Math.floor(i / 3)];
    colrows.push(colrow);
    squares[i] = this.state.xIsNext ? 'X' : 'O';
    this.state.history =  history.concat([{squares}]);
    this.state.stepNumber = history.length;
    this.state.colrows = colrows;
    this.state.xIsNext = !this.state.xIsNext;
    this.updateMoves();
  }
  updateMoves(): void {
    const history: any[] = this.state.history;
    const current = history[this.state.stepNumber];
    this.currentsquares = current.squares;

    const winner = this.calculateWinner(current.squares);
    const colrows = this.state.colrows;

    this.moves = history.map((step,move) => {
        const desc = move ? 'Move #' + move + ' (column ' + colrows[move][0] + ', row ' + + colrows[move][1] + ')' : 'Go to game start';
        const liclass = move === this.state.stepNumber ? 'boldli' : '';
        return ({desc, liclass, move});
    });
    if (winner){
      this.status = 'Winner: ' + winner;
    } else {
        if (this.state.stepNumber >= 9){
          this.status = 'Game is a draw.';
        } else {
          this.status = 'Next player: ' + (this.state.xIsNext ? 'X' : 'O');
        }
    }
    this.boardprops.currentsquares = this.currentsquares;
    this.infoprops = {status: this.status, moves: this.moves};
  }
  jumpTo(step: number): void {
    this.state.stepNumber = step;
    this.state.xIsNext = (step % 2) === 0;
    this.updateMoves();
  }
  resetGame(): void {
    this.state = {
      xIsNext: true,
      stepNumber: 0,
      colrows: [[]],
      history: [{
        squares: Array(9).fill(null),
      }]
    };
    this.currentsquares = Array(9).fill(null);
    this.moves = [];
    this.status = 'Next player: X';
    this.boardprops = {currentsquares: this.currentsquares};
    this.infoprops = {status: this.status, moves: this.moves};
  }
  calculateWinner(squares: any[]) {
    const lines = [
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8],
        [0, 3, 6],
        [1, 4, 7],
        [2, 5, 8],
        [0, 4, 8],
        [2, 4, 6],
    ];
    for (const line of lines){
        const [a, b, c] = line;
        if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
            return squares[a];
        }
    }
    return null;
  }

  constructor() {}
  ngOnInit() {
    this.resetGame();

  }
}
