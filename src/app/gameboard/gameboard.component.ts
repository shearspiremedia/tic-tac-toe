import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-gameboard',
  templateUrl: './gameboard.component.html',
  styleUrls: ['./gameboard.component.css']
})
export class GameboardComponent {
  squarenums: number[] = [0, 1, 2, 3, 4, 5, 6, 7, 8];
  @Input() boardprops: {};
  @Output() squareclicked = new EventEmitter<number>();
  feedback = '';

  squareClick(i: number): void {
    this.feedback = `click on ${i}`;
    this.squareclicked.emit(i);
  }
}
/*
This child component uses both Input and Output decorators from @angular/core.
The Input allows the parent component to pass in the props in the form of boardprops (an object).
The Output allows the child component to transmit user events to the parent component.
The user clicks one of the squares which triggers the squareClick class method of the child component.
That in turns emits an event called squareclicked which is picked up by the parent component along
with its argument that indicates which square was clicked.
*/
